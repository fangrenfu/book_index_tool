﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.IO;

namespace 书籍与目录检索
{
    class BuildBookPage
    {
        mainForm myForm;
        SqlConnection odcConnection;
        string sConnStr;
        string sSitePath;
        int iCurrent;
        int iTotal;
        string sDate;
        delegate void SetTextCallback(string text);//设置委托

        public BuildBookPage(mainForm _myForm,string _sSitePath, string _sConnStr)
        {
            myForm = _myForm;
            sSitePath = _sSitePath;
            odcConnection = new SqlConnection(_sConnStr);
            sConnStr = _sConnStr;
            sDate = DateTime.Now.ToString("yyyy-MM-dd");
            
        }
        /// <summary>
        /// 建立页面
        /// </summary>
        public void BuildPage()
        {
            try
            {
                odcConnection.Open();
                SqlCommand odCommand = odcConnection.CreateCommand();
                //读取总记录条数
                odCommand.CommandText = "select count(*) amount from ls_Book";
                SqlDataReader odrReader;
                odrReader = odCommand.ExecuteReader();
                odrReader.Read();
                iTotal = Convert.ToInt32(odrReader["amount"].ToString());
                odrReader.Close();

                odCommand.CommandText = "select  Book_ID from ls_Book Order by Book_ID";
                odrReader = odCommand.ExecuteReader();
                iCurrent = 0;
                string sBook_ID;
                while (odrReader.Read())
                {
                    iCurrent++;
                    sBook_ID = odrReader["Book_ID"].ToString();
                    GetData.buildBookPage(sBook_ID,sSitePath, sConnStr);
                    setWorkStatus("进度："+iCurrent+"/"+iTotal);
                 
                }
                myForm.btBuildPage.Text = "建立书籍页";

            }
            catch (Exception ex)
            {
                setWorkStatus("建立页面出错"+ex.Message+ex.Data);
            }

        }
        public void BuildSiteMap()
        {
            try
            {
                int PAGESIZE = 5000;
                //建立主要页面的。
                BuildIndexSiteMap();
              
                odcConnection.Open();
                SqlCommand odCommand = odcConnection.CreateCommand();
                //读取总记录条数
                odCommand.CommandText = "select count(*) amount from ls_Book";
                SqlDataReader odrReader;
                odrReader = odCommand.ExecuteReader();
                odrReader.Read();
                iTotal = Convert.ToInt32(odrReader["amount"].ToString());
                odrReader.Close();

                int iPageCount =(int) Math.Ceiling(iTotal / (PAGESIZE*1.0));
         
             
                odCommand.CommandText = "select  Book_ID from ls_Book Order by Book_ID";
                odrReader = odCommand.ExecuteReader();
                iCurrent = 0;
                string sBook_ID;
                //产生一个随机数
                Random ro = new Random();
                for (int i = 0; i < iPageCount; i++)
                {   //当前循环技术
                    int count = 0;
                    string sXmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                    sXmlStr += "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
                    while (odrReader.Read() && count < PAGESIZE)
                    {
                        iCurrent++;
                        count++;
                        sBook_ID = odrReader["Book_ID"].ToString();
                        double prior = ro.Next(5, 10) / 10.0;
                        string file = "http://www.lieshu.net/book/" + (Convert.ToInt32(sBook_ID) / 1000).ToString() + "/"+sBook_ID+".htm";
                        sXmlStr += "<url>";
                        sXmlStr += "<lastmod>" + sDate + "</lastmod>";
                        sXmlStr += "<changefreq>daily</changefreq>";
                        sXmlStr += "<priority>"+prior+"</priority>";
                        sXmlStr += "<loc>"+file+"</loc>";
                        sXmlStr += "</url>";
                        setWorkStatus("进度：" + iCurrent + "/" + iTotal);

                    }
                    sXmlStr += "</urlset>";
                    StreamWriter sw = new StreamWriter(sSitePath +"sitemap/sitemap"+i+".xml", false, Encoding.UTF8);
                    sw.WriteLine(sXmlStr);
                    sw.Close();

                }
            }
            catch(Exception ex)
            {
                setWorkStatus("建立SiteMap出错" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 建立主要页面的Sitemap
        /// </summary>
        /// <param name="sSitePath"></param>
        public void BuildIndexSiteMap()
        {
            string sXmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
            sXmlStr+= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
            //主页
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>1</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/Index.htm</loc>";
            sXmlStr += "</url>";
            //注册页面
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>1</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/Register.aspx</loc>";
            sXmlStr += "</url>";
            //热门更新
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>1</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/LastUpdate.aspx?lb=hot</loc>";
            sXmlStr += "</url>";
            //全部更新
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>1</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/LastUpdate.aspx?lb=all</loc>";
            sXmlStr += "</url>";
            //推荐排行
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>1</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/TopTotalTicket.htm</loc>";
            sXmlStr += "</url>";
            //周排行
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>1</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/TopWeekView.htm</loc>";
            sXmlStr += "</url>";
            //总排行
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>1</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/TopTotalView.htm</loc>";
            sXmlStr += "</url>";
            //帮助页面
            sXmlStr += "<url>";
            sXmlStr += "<lastmod>" + sDate + "</lastmod>";
            sXmlStr += "<changefreq>daily</changefreq>";
            sXmlStr += "<priority>0.9</priority>";
            sXmlStr += "<loc>http://www.lieshu.net/Help.aspx</loc>";
            sXmlStr += "</url>";
            sXmlStr += "</urlset>";
            StreamWriter sw = new StreamWriter(sSitePath + "sitemap/sitemap.xml", false, Encoding.UTF8);
            sw.WriteLine(sXmlStr);
            sw.Close();
        }
        /// <summary>
        /// 设置工作状态
        /// </summary>
        /// <param name="total"></param>
        /// <param name="current"></param>
        /// <param name="btText"></param>
        public void setWorkStatus(string Text)
        {
            //来设置工作状态，解决多线程下不能设置问题
            try
            {
                if (myForm.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(setWorkStatus);
                    myForm.Invoke(d, new object[] { Text });

                }
                else
                {
                      if (Text != "")
                        myForm.lbBuildStatus.Text = Text;

                }
            }
            catch (Exception ex)
            {
                throw (new Exception("设置工作状态时发生异常：" + ex.Message));

            }

        }
    }
}
