﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace 书籍与目录检索
{
    public class ListSearch
    {
        mainForm myForm;
        string sConnStr;
        SqlConnection odcConnection;
        SqlConnection odcConnection2;

        delegate void SetTextCallback(string status,int amount, string message);//设置委托
        /// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="_myForm">窗格</param>
        /// <param name="_iWorkType">搜索方式</param>
        public ListSearch(mainForm _myForm, string _sConnStr)
        {
            myForm=_myForm;           
            odcConnection = new SqlConnection(_sConnStr);
            odcConnection2 = new SqlConnection(_sConnStr);
            sConnStr = _sConnStr;

        }
        /// <summary>
        /// 以多种方式搜索。
        /// </summary>
        public void Search()
        {
            try
            {
                setWorkStatus("获取站点信息", 0,"");
                SiteInfo[] siteInfos = GetData.getAllSiteInfo(sConnStr);

                odcConnection.Open();
                odcConnection2.Open();
                SqlCommand odCommand = odcConnection.CreateCommand();
                SqlCommand odCommand2 = odcConnection.CreateCommand();
                odCommand.CommandText = "select top 100 Book_ID,Book_Name,Book_Writer from ls_Book order by Book_ListTime ";
                odCommand2.CommandText = "update ls_Book set Book_ListTime=getdate() where Book_ID in (select top 100 Book_ID from ls_Book order by Book_ListTime)";
                odCommand2.ExecuteNonQuery();
                SqlDataReader odrReader;
                //建立读取总条数
                odrReader = odCommand.ExecuteReader();
                int bookcount=0;
                while (odrReader.Read())
                {
                    Book book = new Book();
                    book.BookName = odrReader["Book_Name"].ToString();
                  //  book.BookName = "会武功的圣骑士"; //调试代码
                    book.BookID= odrReader["Book_ID"].ToString();
                    book.BookWriter = odrReader["Book_Writer"].ToString();
                    bookcount++;
                    for(int i=0;i<siteInfos.Length;i++)
                    {
                    //    i = 2;  //调试用代码
                        setWorkStatus("第"+bookcount+"本《"+book.BookName+"》，"+siteInfos[i].SiteName+"中查找。", 0, "");

                        string message=GetData.findBookInSite(book, siteInfos[i], sConnStr);
                        if (message != "")
                        {

                            setWorkStatus("完成", 1, "《"+book.BookName+"》"+message);
                        }
                           
                    }
                    
                }



            }
            catch (Exception ex)
            {
                setWorkStatus("", 0, ex.Message);
            }
            finally
            {
                odcConnection.Close();

            }


        }
        public void setWorkStatus(string status,int amount,string message)
        {
            //来设置工作状态，解决多线程下不能设置问题
            try
            {
                if (myForm.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(setWorkStatus);
                    myForm.Invoke(d, new object[] { status, amount,message });

                }
                else
                {
                    if (status != "")
                        myForm.lbListStatus.Text= status;
                    if (amount!=0)
                        myForm.tslListCount.Text = (Convert.ToInt32(myForm.tslListCount.Text) + amount).ToString();
                    if (message != "")
                        myForm.tbListMessage.Text += message + "\r\n";
                }
            }
            catch (Exception ex)
            {
                string info = ex.Message + ex.Data;
               // MessageBox.Show("设置工作状态" + ex.Message + ex.Message + ex.Data);
                setWorkStatus("",0,"设置工作状态错误" +ex.Message + ex.Data + "\r\n");
               
            }

        }
    }
}
