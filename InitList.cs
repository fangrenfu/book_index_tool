﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace 书籍与目录检索
{
    class InitList
    {
        //进度条
        ProgressBar pbInit;
        //连接字符串
        SqlConnection odcConnection;
        SqlConnection odcConnection2;
        mainForm myForm;
        Button btInitList;
        //记录总条数
        int iListCount;
        //窗体
        delegate void SetTextCallback(int total, int  current,string btText);//设置委托
        /// <summary>
        /// 初始化函数
        /// </summary>
        /// <param name="_myForm">窗体</param>
        /// <param name="_pbInit">进度条</param>
        /// <param name="_sConnStr">连接字符串</param>
         public InitList(mainForm _myForm,ProgressBar _pbInit,Button _btInitList, string _sConnStr)
        {
            myForm=_myForm;
            odcConnection = new SqlConnection(_sConnStr);
            odcConnection2 = new SqlConnection(_sConnStr);
            pbInit = _pbInit;
            btInitList = _btInitList;
        }
        /// <summary>
        /// 初始化List的日期
        /// </summary>
         public void InitListDate()
         {
             try
             {
                 odcConnection.Open();
                 odcConnection2.Open();
                 SqlCommand odCommand = odcConnection.CreateCommand();
                 SqlCommand odCommand2 = odcConnection2.CreateCommand();
                 odCommand.CommandText = "select count(*) amount from ls_List";
                 SqlDataReader odrReader;
                 //建立读取总条数
                 odrReader = odCommand.ExecuteReader();
                 odrReader.Read();
                 iListCount = Convert.ToInt32(odrReader["amount"].ToString());
                 odrReader.Close();

                 odCommand.CommandText = "select  List_ID from ls_List order by newid()";
                 odrReader = odCommand.ExecuteReader();
                 int iCurrent = 0;
                 Random r = new Random();
                 int addtion;
                 string sList_ID;
                 while (odrReader.Read())
                 {
                     iCurrent++;
                     sList_ID = odrReader["List_ID"].ToString();
                     addtion = -r.Next(1, 10000);

                     odCommand2.CommandText = "update ls_List set List_LastTime=DATEADD(hour," + addtion + ",List_LastTime) where List_ID= " + sList_ID;
                     odCommand2.ExecuteNonQuery();
                     setWorkStatus(iListCount, iCurrent, "");
                 }
                 setWorkStatus(iListCount, iCurrent, "初始化List");
             }
             catch (Exception ex)
             {
                 throw(new Exception("初始化时候发生异常。"+ex.Message+ex.Data));
             }
             finally
             {
                 odcConnection.Dispose();
                 odcConnection2.Dispose();
                 setWorkStatus(10, 0, "初始化List");
             }

         }

        /// <summary>
        /// 设置进度条
        /// </summary>
        /// <param name="total">总数</param>
        /// <param name="current">当前</param>
         public void setWorkStatus(int total, int current,string btText)
         {
             //来设置工作状态，解决多线程下不能设置问题
             try
             {
                 if (myForm.InvokeRequired)
                 {
                     SetTextCallback d = new SetTextCallback(setWorkStatus);
                     myForm.Invoke(d, new object[] { total, current,btText });

                 }
                 else
                 {       
                     pbInit.Maximum =total;
                     pbInit.Value = current;
                     if (btText != "")
                         btInitList.Text = btText;
                        
                 }
             }
             catch (Exception ex)
             {
                throw(new Exception("设置工作状态时发生异常："+ex.Message));

             }

         }
    }
}
