﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace 书籍与目录检索
{
    //定义多个线程出来。
    
    public partial class mainForm : Form
    {
        Thread threadListNew;// 检索新目录线程
        Thread threadInitList;// 初始化List表线程
        Thread threadBookSearch;//搜索书籍线程
        //搜索章节线程数组
        Thread[] threadHot=new Thread[5];
        Thread[] threadCold = new Thread[5];
        Thread[] threadBookShelf = new Thread[5];
        Thread[] threadFailed = new Thread[5];

        Thread threadBuildPage;
        Thread threadBuildSiteMap;
        //线程类
        ListSearch lsNew;
        InitList initList;
        BookSearch bsBook;
        ChapterSearch csChapter;
        BuildBookPage bpPage;

        int iListInterval;//下次检索需要的总时间
        int iBookInterval;
        //搜索目录开关
        bool bListSearch=false;
        bool bBookSearch=false;
        //章节检索开关
        bool bHotSearch = false;
        bool bColdSearch = false;
        bool bShelfSearch = false;
        bool bFailedSearch = false;
        //搜索次数。
        int iListTimes;
        int iBookTimes;
        int iHotTimes;
        int iColdTimes;
        int iShelfTimes;
        int iFailedTimes;
        string sConnStr = "user id=sa;password=comefirstfangrenfu;initial catalog=kieesoft;Server=60.190.19.121;Connect Timeout=30";

        SqlConnection odcConnection;

        public mainForm()
        {
            InitializeComponent();
            odcConnection = new SqlConnection(sConnStr);

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (bListSearch == true)
            {
                if (threadListNew != null && threadListNew.IsAlive)
                {
                    iListInterval = Convert.ToInt32(tbListInterval.Text) * 60;
                }
                else
                {
                    iListInterval--;
                    lbListStatus.Text = "距离下次开始还有" + iListInterval + "秒";
                    if (iListInterval < 1)
                    {
                        iListTimes++;
                        btSearchList.Text = "第" + iListTimes + "次";
                        searchList();

                    }
                }
            }
            //定期检查书籍搜索状态
            if (bBookSearch == true)
            {
                if (threadBookSearch != null && threadBookSearch.IsAlive)
                {
                    iBookInterval = Convert.ToInt32(tbBookInterval.Text) * 60 * 60;
                    lbBookStatus.Text = "";
                }
                else
                {
                    iBookInterval--;
                    lbBookStatus.Text="距离下次开始还有" + iBookInterval + "秒";
                    if (iBookInterval < 1)
                    {
                        iBookTimes++;
                        btSearchBook.Text = "第" +iBookTimes + "次";
                        searchBook();
                    }
                }
            }            
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (threadListNew != null && threadListNew.IsAlive)
            {
                Thread.Sleep(1000);
                threadListNew.Abort();
            }
        }

        //初始化目录按钮
        private void btInitList_Click(object sender, EventArgs e)
        {
            //线程在运行时候

            if (threadInitList != null && threadInitList.IsAlive)
            {
                threadInitList.Abort();
                btInitList.Text = "开始初始化";
            }
            //没有线程在运行。
            else 
            {
                initList = new InitList(this,pbInitList,btInitList,sConnStr);
                threadInitList = new Thread(new ThreadStart(initList.InitListDate));
                threadInitList.IsBackground = true;
                threadInitList.Start();
                btInitList.Text = "停止初始";
            }
        }
        //清空目录检索记录
        private void btClearListSearch_Click(object sender, EventArgs e)
        {
            tbListMessage.Clear();
        }
        //搜索目录按钮
        private void btSearchList_Click(object sender, EventArgs e)
        {
            //线程在运行时候
            if (bListSearch == true)
            {
                if (threadListNew != null && threadListNew.IsAlive)
                {
                    threadListNew.Abort();
                    Thread.Sleep(500);
                    bListSearch = false;
                    btSearchList.Text = "开始检索";
                    lbListStatus.Text = "就绪";
                }
            }
            else
            {
                bListSearch = true;
                tslListCount.Text = "0";
                iListTimes = 1;
                btSearchList.Text = "第1次";
                searchList();
            }
        }
        //搜索目录函数
        public void searchList()
        {
                lsNew = new ListSearch(this, sConnStr);
                threadListNew = new Thread(new ThreadStart(lsNew.Search));
                threadListNew.Priority = ThreadPriority.AboveNormal;
                threadListNew.IsBackground = true;
                threadListNew.Start();
        }
        private void btBookClear_Click(object sender, EventArgs e)
        {
            tbBookMessage.Clear();
        }
        private void btBookSearch_Click(object sender, EventArgs e)
        {

            if (bBookSearch == true)
            {
                if (threadBookSearch != null && threadBookSearch.IsAlive == true)
                {
                    threadBookSearch.Abort();
                    Thread.Sleep(500);
                }
                bBookSearch = false;
                btSearchBook.Text = "开始检索";
                lbBookStatus.Text = "就绪";
            }
            else
            {
                bBookSearch = true;
                tslBookCount.Text = "0";
                iListTimes = 1;
                btSearchBook.Text = "第1次";
                searchBook();
            }
        }
        //搜索书籍函数
        private void searchBook()
        {
            bsBook = new BookSearch(this, Convert.ToInt32(tbBookForward.Text), sConnStr);
            threadBookSearch = new Thread(new ThreadStart(bsBook.Search));
            threadBookSearch.IsBackground = true;
            threadBookSearch.Start();
            btSearchBook.Text = "停止检索";
        }
        //清空章节检索记录
        private void btClearChapter_Click(object sender, EventArgs e)
        {
            tbChapterMessage.Clear();
        }
        //检索收藏书籍按钮
        private void btBookShelf_Click(object sender, EventArgs e)
        {
            if (bShelfSearch == true)
            {
                bShelfSearch = false;
                btShelf.Text = "收藏";
                udShelf.Enabled = true;
                for (int i = 0; i < udShelf.Value; i++)
                {
                    if (threadBookShelf[i] != null && threadBookShelf[i].IsAlive == true)
                    {
                        threadBookShelf[i].Abort();
                        Thread.Sleep(100);
                    }
                }
            }
            else
            {
                bShelfSearch = true;
                udShelf.Enabled = false;
                iShelfTimes = 0;
                for (int i = 0; i < udShelf.Value; i++)
                {
                    iShelfTimes++;
                    btShelf.Text = "第" + iShelfTimes + "次";
                    Thread.Sleep(500);
                    searchBookShelf(ref threadBookShelf[i]);
                }
            }
        }
        //更新收藏夹中的那些  ,必须使用ref标签让其变传递地址
        private void searchBookShelf(ref Thread thread)
        {

            try
            {
                csChapter = new ChapterSearch(this, lbShelfStatus, lbShelfFind, sConnStr, ChapterSearchType.BookShelf);
                thread = new Thread(new ThreadStart(csChapter.Search));
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("获得BookSites出错" + ex.Message + ex.Data);
            }

        }
        private void btHot_Click(object sender, EventArgs e)
        {
            if (bHotSearch == true)
            {
                bHotSearch  = false;
                btHot.Text = "热门";
                udHot.Enabled = true;
                for (int i = 0; i < udHot.Value; i++)
                {
                    if (threadHot[i] != null && threadHot[i].IsAlive == true)
                    {
                        threadHot[i].Abort();
                        Thread.Sleep(100);
                    }
                }
            }
            else
            {
                bHotSearch  = true;
                udHot.Enabled = false;
                iHotTimes = 0;
                for (int i = 0; i < udHot.Value; i++)
                {
                    iHotTimes++; ;
                    btHot.Text = "第" + iHotTimes + "次";
                    Thread.Sleep(500);
                    searchHot(ref threadHot[i]);
                }
            }
        }
        //热门按钮
        private void searchHot(ref Thread thread)
        {
            try
            {
                csChapter = new ChapterSearch(this, lbHotStatus, lbHotFind, sConnStr, ChapterSearchType.Hot);
                thread = new Thread(new ThreadStart(csChapter.Search));
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("搜索热门章节出错"+ex.Data+ex.Message);
            }
        }
        //完本按钮
        private void btCold_Click(object sender, EventArgs e)
        {
            if (bColdSearch == true)
            {
                bColdSearch = false;
                btCold.Text = "完本";
                udCold.Enabled = true;
                for (int i = 0; i < udCold.Value; i++)
                {
                    if (threadCold[i] != null && threadCold[i].IsAlive == true)
                    {
                        threadCold[i].Abort();
                        Thread.Sleep(100);
                    }
                }
            }
            else
            {
                bColdSearch = true;
                btCold.Text = "第1次";
                udCold.Enabled = false;
                iColdTimes = 0;
                for (int i = 0; i < udCold.Value; i++)
                {
                    iColdTimes++; ;
                    btCold.Text = "第" + iColdTimes + "次";
                    Thread.Sleep(500);
                    searchCold(ref threadCold[i]);
                }
            }
        }
        //
        private void searchCold(ref Thread thread)
        {

            try
            {
                csChapter = new ChapterSearch(this, lbColdStatus, lbColdFind, sConnStr, ChapterSearchType.Cold);
                thread = new Thread(new ThreadStart(csChapter.Search));
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("搜索完本章节出错"+ex.Message+ex.Data);
            }
        }
        //错误按钮
        private void btFailed_Click(object sender, EventArgs e)
        {
            if (bFailedSearch== true)
            {
                bFailedSearch = false;
                btFailed.Text = "错误";
                udFailed.Enabled = true;
                for (int i = 0; i < udFailed.Value; i++)
                {
                    if (threadFailed[i] != null && threadFailed[i].IsAlive == true)
                    {
                        threadFailed[i].Abort();
                        Thread.Sleep(100);
                    }
                }
            }
            else
            {
                iFailedTimes = 0;
                bFailedSearch = true;
                udFailed.Enabled = false;
                for (int i = 0; i < udFailed.Value; i++)
                {
                    btFailed.Text = "第" + iFailedTimes + "次";
                    Thread.Sleep(500);
                    searchFailed(ref threadFailed[i]);
                }
            }
        }
        private void searchFailed(ref Thread thread)
        {
            try
            {
                csChapter = new ChapterSearch(this, lbFailedStatus, lbFailedFind, sConnStr, ChapterSearchType.Failed);
                thread = new Thread(new ThreadStart(csChapter.Search));
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show("搜索失败章节出错"+ex.Message+ex.Data);
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            lbChapterList.Text = GetData.getListStatus(sConnStr);
            
            if (bShelfSearch == true)
            {
                for (int i = 0; i < udShelf.Value; i++)
                {

                    if (threadBookShelf[i] == null || threadBookShelf[i].IsAlive == false)
                    {
                        iShelfTimes++;
                        btShelf.Text = "第" + iShelfTimes + "次";
                        searchBookShelf(ref threadBookShelf[i]);
                        Thread.Sleep(500);
                    }
                }
            }

            if (bHotSearch == true)
            {
                for (int i = 0; i < udHot.Value; i++)
                {
                    if (threadHot[i] == null || threadHot[i].IsAlive == false)
                    {
                        iHotTimes++;
                        btHot.Text = "第" + iHotTimes + "次";
                        searchHot(ref threadHot[i]);
                        Thread.Sleep(500);
                    }
                }
            }

            if (bColdSearch == true)
            {
                for (int i = 0; i < udCold.Value; i++)
                {

                    if (threadCold[i] == null || threadCold[i].IsAlive == false)
                    {
                        iColdTimes++;
                        btCold.Text = "第" + iColdTimes + "次";
                        searchCold(ref threadCold[i]);
                        Thread.Sleep(500);
                    }
                }
            }

            if (bFailedSearch == true)
            {
                for (int i = 0; i < udFailed.Value; i++)
                {
                    if (threadFailed[i] == null || threadFailed[i].IsAlive == false)
                    {
                        iFailedTimes++;
                        btFailed.Text = "第" + iFailedTimes + "次";
                        searchFailed(ref threadFailed[i]);
                        Thread.Sleep(500);
                    }
                }
            }
        }

        private void btBuildPage_Click(object sender, EventArgs e)
        {

            if (threadBuildPage != null && threadBuildPage.IsAlive)
            {
                threadBuildPage.Abort();
                Thread.Sleep(200);
                btBuildPage.Text= "建立书籍页";
            }

            else
            {
                bpPage = new BuildBookPage(this,tbSitePath.Text,sConnStr);
                threadBuildPage = new Thread(new ThreadStart(bpPage.BuildPage));
                threadBuildPage.IsBackground = true;
                threadBuildPage.Start();
            }
        }

        private void btBuildSiteMap_Click(object sender, EventArgs e)
        {
            if (threadBuildSiteMap != null && threadBuildSiteMap.IsAlive)
            {
                threadBuildSiteMap.Abort();
                Thread.Sleep(200);
                btBuildSiteMap.Text = "生成SiteMap";
            }

            else
            {
                bpPage = new BuildBookPage(this, tbSitePath.Text, sConnStr);
                threadBuildSiteMap = new Thread(new ThreadStart(bpPage.BuildSiteMap));
                threadBuildSiteMap.IsBackground = true;
                threadBuildSiteMap.Start();
            }
        }
    }

}
