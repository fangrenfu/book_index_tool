﻿namespace 书籍与目录检索
{
    partial class mainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.secondTimer = new System.Windows.Forms.Timer(this.components);
            this.tabListSearch = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.tbListInterval = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbListStatus = new System.Windows.Forms.Label();
            this.btListClear = new System.Windows.Forms.Button();
            this.tbListMessage = new System.Windows.Forms.TextBox();
            this.btSearchList = new System.Windows.Forms.Button();
            this.tabMySearch = new System.Windows.Forms.TabControl();
            this.tabBookSearch = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.tbBookInterval = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbBookStatus = new System.Windows.Forms.Label();
            this.btBookClear = new System.Windows.Forms.Button();
            this.tbBookForward = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btSearchBook = new System.Windows.Forms.Button();
            this.tbBookMessage = new System.Windows.Forms.TextBox();
            this.tabChapterSearch = new System.Windows.Forms.TabPage();
            this.lbChapterList = new System.Windows.Forms.Label();
            this.lbFailedStatus = new System.Windows.Forms.Label();
            this.lbShelfStatus = new System.Windows.Forms.Label();
            this.lbColdStatus = new System.Windows.Forms.Label();
            this.lbFailedFind = new System.Windows.Forms.Label();
            this.lbHotFind = new System.Windows.Forms.Label();
            this.lbColdFind = new System.Windows.Forms.Label();
            this.lbShelfFind = new System.Windows.Forms.Label();
            this.lbHotStatus = new System.Windows.Forms.Label();
            this.btClearChapter = new System.Windows.Forms.Button();
            this.udFailed = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.btFailed = new System.Windows.Forms.Button();
            this.udShelf = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.btShelf = new System.Windows.Forms.Button();
            this.udCold = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.btCold = new System.Windows.Forms.Button();
            this.udHot = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btHot = new System.Windows.Forms.Button();
            this.tbChapterMessage = new System.Windows.Forms.TextBox();
            this.tabImageCheck = new System.Windows.Forms.TabPage();
            this.tabSetting = new System.Windows.Forms.TabPage();
            this.tbSitePath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lbBuildStatus = new System.Windows.Forms.Label();
            this.btBuildSiteMap = new System.Windows.Forms.Button();
            this.btBuildPage = new System.Windows.Forms.Button();
            this.pbInitList = new System.Windows.Forms.ProgressBar();
            this.lbInit = new System.Windows.Forms.Label();
            this.btInitList = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslBookCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslListCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslChapterCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.chapterTimer = new System.Windows.Forms.Timer(this.components);
            this.tabListSearch.SuspendLayout();
            this.tabMySearch.SuspendLayout();
            this.tabBookSearch.SuspendLayout();
            this.tabChapterSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udFailed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udShelf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udHot)).BeginInit();
            this.tabSetting.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // secondTimer
            // 
            this.secondTimer.Enabled = true;
            this.secondTimer.Interval = 1000;
            this.secondTimer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabListSearch
            // 
            this.tabListSearch.Controls.Add(this.label3);
            this.tabListSearch.Controls.Add(this.tbListInterval);
            this.tabListSearch.Controls.Add(this.label2);
            this.tabListSearch.Controls.Add(this.lbListStatus);
            this.tabListSearch.Controls.Add(this.btListClear);
            this.tabListSearch.Controls.Add(this.tbListMessage);
            this.tabListSearch.Controls.Add(this.btSearchList);
            this.tabListSearch.Location = new System.Drawing.Point(4, 21);
            this.tabListSearch.Name = "tabListSearch";
            this.tabListSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabListSearch.Size = new System.Drawing.Size(579, 424);
            this.tabListSearch.TabIndex = 1;
            this.tabListSearch.Text = "目录检索";
            this.tabListSearch.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(121, 364);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "分钟";
            // 
            // tbListInterval
            // 
            this.tbListInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbListInterval.Location = new System.Drawing.Point(95, 360);
            this.tbListInterval.Name = "tbListInterval";
            this.tbListInterval.Size = new System.Drawing.Size(20, 21);
            this.tbListInterval.TabIndex = 7;
            this.tbListInterval.Text = "0";
            this.tbListInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 364);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "间隔";
            // 
            // lbListStatus
            // 
            this.lbListStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbListStatus.AutoSize = true;
            this.lbListStatus.Location = new System.Drawing.Point(28, 400);
            this.lbListStatus.Name = "lbListStatus";
            this.lbListStatus.Size = new System.Drawing.Size(29, 12);
            this.lbListStatus.TabIndex = 4;
            this.lbListStatus.Text = "就绪";
            // 
            // btListClear
            // 
            this.btListClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btListClear.Location = new System.Drawing.Point(378, 359);
            this.btListClear.Name = "btListClear";
            this.btListClear.Size = new System.Drawing.Size(75, 23);
            this.btListClear.TabIndex = 2;
            this.btListClear.Text = "清除记录";
            this.btListClear.UseVisualStyleBackColor = true;
            this.btListClear.Click += new System.EventHandler(this.btClearListSearch_Click);
            // 
            // tbListMessage
            // 
            this.tbListMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbListMessage.Location = new System.Drawing.Point(20, 20);
            this.tbListMessage.Multiline = true;
            this.tbListMessage.Name = "tbListMessage";
            this.tbListMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbListMessage.Size = new System.Drawing.Size(536, 320);
            this.tbListMessage.TabIndex = 1;
            // 
            // btSearchList
            // 
            this.btSearchList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btSearchList.Location = new System.Drawing.Point(208, 359);
            this.btSearchList.Name = "btSearchList";
            this.btSearchList.Size = new System.Drawing.Size(75, 23);
            this.btSearchList.TabIndex = 0;
            this.btSearchList.Text = "开始检索";
            this.btSearchList.UseVisualStyleBackColor = true;
            this.btSearchList.Click += new System.EventHandler(this.btSearchList_Click);
            // 
            // tabMySearch
            // 
            this.tabMySearch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMySearch.Controls.Add(this.tabBookSearch);
            this.tabMySearch.Controls.Add(this.tabListSearch);
            this.tabMySearch.Controls.Add(this.tabChapterSearch);
            this.tabMySearch.Controls.Add(this.tabImageCheck);
            this.tabMySearch.Controls.Add(this.tabSetting);
            this.tabMySearch.Location = new System.Drawing.Point(12, 12);
            this.tabMySearch.Name = "tabMySearch";
            this.tabMySearch.SelectedIndex = 0;
            this.tabMySearch.Size = new System.Drawing.Size(587, 449);
            this.tabMySearch.TabIndex = 1;
            // 
            // tabBookSearch
            // 
            this.tabBookSearch.Controls.Add(this.label6);
            this.tabBookSearch.Controls.Add(this.tbBookInterval);
            this.tabBookSearch.Controls.Add(this.label4);
            this.tabBookSearch.Controls.Add(this.lbBookStatus);
            this.tabBookSearch.Controls.Add(this.btBookClear);
            this.tabBookSearch.Controls.Add(this.tbBookForward);
            this.tabBookSearch.Controls.Add(this.label5);
            this.tabBookSearch.Controls.Add(this.btSearchBook);
            this.tabBookSearch.Controls.Add(this.tbBookMessage);
            this.tabBookSearch.Location = new System.Drawing.Point(4, 21);
            this.tabBookSearch.Name = "tabBookSearch";
            this.tabBookSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabBookSearch.Size = new System.Drawing.Size(579, 424);
            this.tabBookSearch.TabIndex = 0;
            this.tabBookSearch.Text = "书籍检索";
            this.tabBookSearch.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(118, 364);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "小时";
            // 
            // tbBookInterval
            // 
            this.tbBookInterval.Location = new System.Drawing.Point(82, 360);
            this.tbBookInterval.Name = "tbBookInterval";
            this.tbBookInterval.Size = new System.Drawing.Size(30, 21);
            this.tbBookInterval.TabIndex = 12;
            this.tbBookInterval.Text = "6";
            this.tbBookInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 364);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "间隔";
            // 
            // lbBookStatus
            // 
            this.lbBookStatus.AutoSize = true;
            this.lbBookStatus.Location = new System.Drawing.Point(18, 400);
            this.lbBookStatus.Name = "lbBookStatus";
            this.lbBookStatus.Size = new System.Drawing.Size(29, 12);
            this.lbBookStatus.TabIndex = 10;
            this.lbBookStatus.Text = "就绪";
            // 
            // btBookClear
            // 
            this.btBookClear.Location = new System.Drawing.Point(447, 359);
            this.btBookClear.Name = "btBookClear";
            this.btBookClear.Size = new System.Drawing.Size(75, 23);
            this.btBookClear.TabIndex = 9;
            this.btBookClear.Text = "清除记录";
            this.btBookClear.UseVisualStyleBackColor = true;
            this.btBookClear.Click += new System.EventHandler(this.btBookClear_Click);
            // 
            // tbBookForward
            // 
            this.tbBookForward.Location = new System.Drawing.Point(249, 360);
            this.tbBookForward.Name = "tbBookForward";
            this.tbBookForward.Size = new System.Drawing.Size(51, 21);
            this.tbBookForward.TabIndex = 8;
            this.tbBookForward.Text = "200";
            this.tbBookForward.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(178, 364);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "成功页推后";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btSearchBook
            // 
            this.btSearchBook.Location = new System.Drawing.Point(335, 359);
            this.btSearchBook.Name = "btSearchBook";
            this.btSearchBook.Size = new System.Drawing.Size(75, 23);
            this.btSearchBook.TabIndex = 6;
            this.btSearchBook.Text = "开始检索";
            this.btSearchBook.UseVisualStyleBackColor = true;
            this.btSearchBook.Click += new System.EventHandler(this.btBookSearch_Click);
            // 
            // tbBookMessage
            // 
            this.tbBookMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBookMessage.Location = new System.Drawing.Point(20, 20);
            this.tbBookMessage.Multiline = true;
            this.tbBookMessage.Name = "tbBookMessage";
            this.tbBookMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbBookMessage.Size = new System.Drawing.Size(536, 320);
            this.tbBookMessage.TabIndex = 2;
            // 
            // tabChapterSearch
            // 
            this.tabChapterSearch.Controls.Add(this.lbChapterList);
            this.tabChapterSearch.Controls.Add(this.lbFailedStatus);
            this.tabChapterSearch.Controls.Add(this.lbShelfStatus);
            this.tabChapterSearch.Controls.Add(this.lbColdStatus);
            this.tabChapterSearch.Controls.Add(this.lbFailedFind);
            this.tabChapterSearch.Controls.Add(this.lbHotFind);
            this.tabChapterSearch.Controls.Add(this.lbColdFind);
            this.tabChapterSearch.Controls.Add(this.lbShelfFind);
            this.tabChapterSearch.Controls.Add(this.lbHotStatus);
            this.tabChapterSearch.Controls.Add(this.btClearChapter);
            this.tabChapterSearch.Controls.Add(this.udFailed);
            this.tabChapterSearch.Controls.Add(this.label9);
            this.tabChapterSearch.Controls.Add(this.btFailed);
            this.tabChapterSearch.Controls.Add(this.udShelf);
            this.tabChapterSearch.Controls.Add(this.label8);
            this.tabChapterSearch.Controls.Add(this.btShelf);
            this.tabChapterSearch.Controls.Add(this.udCold);
            this.tabChapterSearch.Controls.Add(this.label7);
            this.tabChapterSearch.Controls.Add(this.btCold);
            this.tabChapterSearch.Controls.Add(this.udHot);
            this.tabChapterSearch.Controls.Add(this.label1);
            this.tabChapterSearch.Controls.Add(this.btHot);
            this.tabChapterSearch.Controls.Add(this.tbChapterMessage);
            this.tabChapterSearch.Location = new System.Drawing.Point(4, 21);
            this.tabChapterSearch.Name = "tabChapterSearch";
            this.tabChapterSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabChapterSearch.Size = new System.Drawing.Size(579, 424);
            this.tabChapterSearch.TabIndex = 4;
            this.tabChapterSearch.Text = "章节更新";
            this.tabChapterSearch.UseVisualStyleBackColor = true;
            // 
            // lbChapterList
            // 
            this.lbChapterList.AutoSize = true;
            this.lbChapterList.Location = new System.Drawing.Point(27, 397);
            this.lbChapterList.Name = "lbChapterList";
            this.lbChapterList.Size = new System.Drawing.Size(53, 12);
            this.lbChapterList.TabIndex = 26;
            this.lbChapterList.Text = "条目统计";
            // 
            // lbFailedStatus
            // 
            this.lbFailedStatus.AutoSize = true;
            this.lbFailedStatus.Location = new System.Drawing.Point(368, 321);
            this.lbFailedStatus.Name = "lbFailedStatus";
            this.lbFailedStatus.Size = new System.Drawing.Size(29, 12);
            this.lbFailedStatus.TabIndex = 25;
            this.lbFailedStatus.Text = "就绪";
            // 
            // lbShelfStatus
            // 
            this.lbShelfStatus.AutoSize = true;
            this.lbShelfStatus.Location = new System.Drawing.Point(368, 232);
            this.lbShelfStatus.Name = "lbShelfStatus";
            this.lbShelfStatus.Size = new System.Drawing.Size(29, 12);
            this.lbShelfStatus.TabIndex = 24;
            this.lbShelfStatus.Text = "就绪";
            // 
            // lbColdStatus
            // 
            this.lbColdStatus.AutoSize = true;
            this.lbColdStatus.Location = new System.Drawing.Point(368, 151);
            this.lbColdStatus.Name = "lbColdStatus";
            this.lbColdStatus.Size = new System.Drawing.Size(29, 12);
            this.lbColdStatus.TabIndex = 23;
            this.lbColdStatus.Text = "就绪";
            // 
            // lbFailedFind
            // 
            this.lbFailedFind.AutoSize = true;
            this.lbFailedFind.Location = new System.Drawing.Point(545, 280);
            this.lbFailedFind.Name = "lbFailedFind";
            this.lbFailedFind.Size = new System.Drawing.Size(11, 12);
            this.lbFailedFind.TabIndex = 22;
            this.lbFailedFind.Text = "0";
            // 
            // lbHotFind
            // 
            this.lbHotFind.AutoSize = true;
            this.lbHotFind.Location = new System.Drawing.Point(545, 25);
            this.lbHotFind.Name = "lbHotFind";
            this.lbHotFind.Size = new System.Drawing.Size(11, 12);
            this.lbHotFind.TabIndex = 21;
            this.lbHotFind.Text = "0";
            // 
            // lbColdFind
            // 
            this.lbColdFind.AutoSize = true;
            this.lbColdFind.Location = new System.Drawing.Point(545, 109);
            this.lbColdFind.Name = "lbColdFind";
            this.lbColdFind.Size = new System.Drawing.Size(11, 12);
            this.lbColdFind.TabIndex = 20;
            this.lbColdFind.Text = "0";
            // 
            // lbShelfFind
            // 
            this.lbShelfFind.AutoSize = true;
            this.lbShelfFind.Location = new System.Drawing.Point(545, 192);
            this.lbShelfFind.Name = "lbShelfFind";
            this.lbShelfFind.Size = new System.Drawing.Size(11, 12);
            this.lbShelfFind.TabIndex = 19;
            this.lbShelfFind.Text = "0";
            // 
            // lbHotStatus
            // 
            this.lbHotStatus.AutoSize = true;
            this.lbHotStatus.Location = new System.Drawing.Point(368, 65);
            this.lbHotStatus.Name = "lbHotStatus";
            this.lbHotStatus.Size = new System.Drawing.Size(29, 12);
            this.lbHotStatus.TabIndex = 16;
            this.lbHotStatus.Text = "就绪";
            // 
            // btClearChapter
            // 
            this.btClearChapter.Location = new System.Drawing.Point(425, 360);
            this.btClearChapter.Name = "btClearChapter";
            this.btClearChapter.Size = new System.Drawing.Size(75, 23);
            this.btClearChapter.TabIndex = 13;
            this.btClearChapter.Text = "清除记录";
            this.btClearChapter.UseVisualStyleBackColor = true;
            this.btClearChapter.Click += new System.EventHandler(this.btClearChapter_Click);
            // 
            // udFailed
            // 
            this.udFailed.Location = new System.Drawing.Point(425, 275);
            this.udFailed.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udFailed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udFailed.Name = "udFailed";
            this.udFailed.Size = new System.Drawing.Size(37, 21);
            this.udFailed.TabIndex = 12;
            this.udFailed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udFailed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(368, 279);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 11;
            this.label9.Text = "线程数：";
            // 
            // btFailed
            // 
            this.btFailed.Location = new System.Drawing.Point(479, 274);
            this.btFailed.Name = "btFailed";
            this.btFailed.Size = new System.Drawing.Size(60, 23);
            this.btFailed.TabIndex = 10;
            this.btFailed.Text = "错误";
            this.btFailed.UseVisualStyleBackColor = true;
            this.btFailed.Click += new System.EventHandler(this.btFailed_Click);
            // 
            // udShelf
            // 
            this.udShelf.Location = new System.Drawing.Point(425, 188);
            this.udShelf.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udShelf.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udShelf.Name = "udShelf";
            this.udShelf.Size = new System.Drawing.Size(37, 21);
            this.udShelf.TabIndex = 9;
            this.udShelf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udShelf.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(368, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 8;
            this.label8.Text = "线程数：";
            // 
            // btShelf
            // 
            this.btShelf.Location = new System.Drawing.Point(479, 187);
            this.btShelf.Name = "btShelf";
            this.btShelf.Size = new System.Drawing.Size(60, 23);
            this.btShelf.TabIndex = 7;
            this.btShelf.Text = "收藏";
            this.btShelf.UseVisualStyleBackColor = true;
            this.btShelf.Click += new System.EventHandler(this.btBookShelf_Click);
            // 
            // udCold
            // 
            this.udCold.Location = new System.Drawing.Point(425, 105);
            this.udCold.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udCold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udCold.Name = "udCold";
            this.udCold.Size = new System.Drawing.Size(37, 21);
            this.udCold.TabIndex = 6;
            this.udCold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udCold.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(368, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "线程数：";
            // 
            // btCold
            // 
            this.btCold.Location = new System.Drawing.Point(479, 104);
            this.btCold.Name = "btCold";
            this.btCold.Size = new System.Drawing.Size(60, 23);
            this.btCold.TabIndex = 4;
            this.btCold.Text = "完本";
            this.btCold.UseVisualStyleBackColor = true;
            this.btCold.Click += new System.EventHandler(this.btCold_Click);
            // 
            // udHot
            // 
            this.udHot.Location = new System.Drawing.Point(425, 21);
            this.udHot.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.udHot.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udHot.Name = "udHot";
            this.udHot.Size = new System.Drawing.Size(37, 21);
            this.udHot.TabIndex = 3;
            this.udHot.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udHot.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(368, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "线程数：";
            // 
            // btHot
            // 
            this.btHot.Location = new System.Drawing.Point(479, 20);
            this.btHot.Name = "btHot";
            this.btHot.Size = new System.Drawing.Size(60, 23);
            this.btHot.TabIndex = 1;
            this.btHot.Text = "热门";
            this.btHot.UseVisualStyleBackColor = true;
            this.btHot.Click += new System.EventHandler(this.btHot_Click);
            // 
            // tbChapterMessage
            // 
            this.tbChapterMessage.Location = new System.Drawing.Point(20, 20);
            this.tbChapterMessage.Multiline = true;
            this.tbChapterMessage.Name = "tbChapterMessage";
            this.tbChapterMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbChapterMessage.Size = new System.Drawing.Size(332, 363);
            this.tbChapterMessage.TabIndex = 0;
            // 
            // tabImageCheck
            // 
            this.tabImageCheck.Location = new System.Drawing.Point(4, 21);
            this.tabImageCheck.Name = "tabImageCheck";
            this.tabImageCheck.Size = new System.Drawing.Size(579, 424);
            this.tabImageCheck.TabIndex = 2;
            this.tabImageCheck.Text = "封面检测";
            this.tabImageCheck.UseVisualStyleBackColor = true;
            // 
            // tabSetting
            // 
            this.tabSetting.Controls.Add(this.tbSitePath);
            this.tabSetting.Controls.Add(this.label11);
            this.tabSetting.Controls.Add(this.lbBuildStatus);
            this.tabSetting.Controls.Add(this.btBuildSiteMap);
            this.tabSetting.Controls.Add(this.btBuildPage);
            this.tabSetting.Controls.Add(this.pbInitList);
            this.tabSetting.Controls.Add(this.lbInit);
            this.tabSetting.Controls.Add(this.btInitList);
            this.tabSetting.Location = new System.Drawing.Point(4, 21);
            this.tabSetting.Name = "tabSetting";
            this.tabSetting.Padding = new System.Windows.Forms.Padding(3);
            this.tabSetting.Size = new System.Drawing.Size(579, 424);
            this.tabSetting.TabIndex = 3;
            this.tabSetting.Text = "系统设置";
            this.tabSetting.UseVisualStyleBackColor = true;
            // 
            // tbSitePath
            // 
            this.tbSitePath.Location = new System.Drawing.Point(87, 87);
            this.tbSitePath.Name = "tbSitePath";
            this.tbSitePath.Size = new System.Drawing.Size(164, 21);
            this.tbSitePath.TabIndex = 7;
            this.tbSitePath.Text = "d:\\";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(40, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 6;
            this.label11.Text = "路径：";
            // 
            // lbBuildStatus
            // 
            this.lbBuildStatus.AutoSize = true;
            this.lbBuildStatus.Location = new System.Drawing.Point(257, 91);
            this.lbBuildStatus.Name = "lbBuildStatus";
            this.lbBuildStatus.Size = new System.Drawing.Size(41, 12);
            this.lbBuildStatus.TabIndex = 5;
            this.lbBuildStatus.Text = "进度：";
            // 
            // btBuildSiteMap
            // 
            this.btBuildSiteMap.Location = new System.Drawing.Point(460, 86);
            this.btBuildSiteMap.Name = "btBuildSiteMap";
            this.btBuildSiteMap.Size = new System.Drawing.Size(81, 23);
            this.btBuildSiteMap.TabIndex = 4;
            this.btBuildSiteMap.Text = "生成SiteMap";
            this.btBuildSiteMap.UseVisualStyleBackColor = true;
            this.btBuildSiteMap.Click += new System.EventHandler(this.btBuildSiteMap_Click);
            // 
            // btBuildPage
            // 
            this.btBuildPage.Location = new System.Drawing.Point(379, 86);
            this.btBuildPage.Name = "btBuildPage";
            this.btBuildPage.Size = new System.Drawing.Size(75, 23);
            this.btBuildPage.TabIndex = 3;
            this.btBuildPage.Text = "建立书籍页";
            this.btBuildPage.UseVisualStyleBackColor = true;
            this.btBuildPage.Click += new System.EventHandler(this.btBuildPage_Click);
            // 
            // pbInitList
            // 
            this.pbInitList.Location = new System.Drawing.Point(87, 25);
            this.pbInitList.Name = "pbInitList";
            this.pbInitList.Size = new System.Drawing.Size(346, 23);
            this.pbInitList.TabIndex = 2;
            // 
            // lbInit
            // 
            this.lbInit.AutoSize = true;
            this.lbInit.Location = new System.Drawing.Point(40, 31);
            this.lbInit.Name = "lbInit";
            this.lbInit.Size = new System.Drawing.Size(41, 12);
            this.lbInit.TabIndex = 1;
            this.lbInit.Text = "进度：";
            // 
            // btInitList
            // 
            this.btInitList.Location = new System.Drawing.Point(460, 26);
            this.btInitList.Name = "btInitList";
            this.btInitList.Size = new System.Drawing.Size(81, 22);
            this.btInitList.TabIndex = 0;
            this.btInitList.Text = "初始化List";
            this.btInitList.UseVisualStyleBackColor = true;
            this.btInitList.Click += new System.EventHandler(this.btInitList_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslBookCount,
            this.toolStripStatusLabel2,
            this.tslListCount,
            this.toolStripStatusLabel3,
            this.tslChapterCount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 464);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(611, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(41, 17);
            this.toolStripStatusLabel1.Text = "书籍：";
            // 
            // tslBookCount
            // 
            this.tslBookCount.Name = "tslBookCount";
            this.tslBookCount.Size = new System.Drawing.Size(11, 17);
            this.tslBookCount.Text = "0";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(53, 17);
            this.toolStripStatusLabel2.Text = "，目录：";
            // 
            // tslListCount
            // 
            this.tslListCount.Name = "tslListCount";
            this.tslListCount.Size = new System.Drawing.Size(11, 17);
            this.tslListCount.Text = "0";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(53, 17);
            this.toolStripStatusLabel3.Text = "，章节：";
            // 
            // tslChapterCount
            // 
            this.tslChapterCount.Name = "tslChapterCount";
            this.tslChapterCount.Size = new System.Drawing.Size(11, 17);
            this.tslChapterCount.Text = "0";
            // 
            // chapterTimer
            // 
            this.chapterTimer.Enabled = true;
            this.chapterTimer.Interval = 3000;
            this.chapterTimer.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 486);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabMySearch);
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "书籍与目录检索";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.tabListSearch.ResumeLayout(false);
            this.tabListSearch.PerformLayout();
            this.tabMySearch.ResumeLayout(false);
            this.tabBookSearch.ResumeLayout(false);
            this.tabBookSearch.PerformLayout();
            this.tabChapterSearch.ResumeLayout(false);
            this.tabChapterSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udFailed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udShelf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udHot)).EndInit();
            this.tabSetting.ResumeLayout(false);
            this.tabSetting.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer secondTimer;
        private System.Windows.Forms.TabPage tabListSearch;
        private System.Windows.Forms.TabControl tabMySearch;
        private System.Windows.Forms.TabPage tabBookSearch;
        private System.Windows.Forms.Button btListClear;
        private System.Windows.Forms.TabPage tabImageCheck;
        private System.Windows.Forms.TabPage tabSetting;
        private System.Windows.Forms.TabPage tabChapterSearch;
        private System.Windows.Forms.Button btInitList;
        private System.Windows.Forms.ProgressBar pbInitList;
        private System.Windows.Forms.Label lbInit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbListInterval;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btBookClear;
        private System.Windows.Forms.TextBox tbBookForward;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbBookInterval;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown udHot;
        private System.Windows.Forms.NumericUpDown udCold;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown udShelf;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown udFailed;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btClearChapter;
        public System.Windows.Forms.TextBox tbChapterMessage;
        public System.Windows.Forms.Button btHot;
        public System.Windows.Forms.Button btCold;
        public System.Windows.Forms.Button btShelf;
        public System.Windows.Forms.Button btFailed;
        public System.Windows.Forms.TextBox tbListMessage;
        public System.Windows.Forms.Button btSearchList;
        public System.Windows.Forms.TextBox tbBookMessage;
        public System.Windows.Forms.Button btSearchBook;
        public System.Windows.Forms.ToolStripStatusLabel tslBookCount;
        public System.Windows.Forms.ToolStripStatusLabel tslListCount;
        public System.Windows.Forms.ToolStripStatusLabel tslChapterCount;
        public System.Windows.Forms.Label lbHotStatus;
        public System.Windows.Forms.Label lbBookStatus;
        public System.Windows.Forms.Label lbListStatus;
        public System.Windows.Forms.Label lbFailedStatus;
        public System.Windows.Forms.Label lbShelfStatus;
        public System.Windows.Forms.Label lbColdStatus;
        private System.Windows.Forms.Label lbShelfFind;
        private System.Windows.Forms.Label lbFailedFind;
        private System.Windows.Forms.Label lbHotFind;
        private System.Windows.Forms.Label lbColdFind;
        private System.Windows.Forms.Label lbChapterList;
        private System.Windows.Forms.Timer chapterTimer;
        private System.Windows.Forms.Button btBuildSiteMap;
        private System.Windows.Forms.TextBox tbSitePath;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label lbBuildStatus;
        public System.Windows.Forms.Button btBuildPage;

    }
}

