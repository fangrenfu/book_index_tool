﻿using System;

/// <summary>
///RegList 的摘要说明
/// </summary>
public class RegList
{
	public RegList()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}
    /// <summary>
    /// 章节标题
    /// </summary>
    public string RegTitle
    {
        get;
        set;
    }
    /// <summary>
    /// 章节内容
    /// </summary>

    public string RegContent
    {
        get;
        set;
    }
    /// <summary>
    /// 列表中书籍名称
    /// </summary>
    public string RegBookName
    {
        get;
        set;
    }
    /// <summary>
    /// 获得列表的表达式
    /// </summary>
    public string RegChapterList
    {
        get;
        set;
    }
    /// <summary>
    /// 站点名称
    /// </summary>
    public string SiteName
    {
        get;
        set;
    }
    /// <summary>
    /// 需要去掉的字符
    /// </summary>
    public string RegExcept
    {
        get;
        set;
    }
    /// <summary>
    /// 默认首页
    /// </summary>
    public string DefaultIndex
    {
        get;
        set;
    }

}
/// <summary>
/// 导航
/// </summary>
public class GuideURL
{
    public GuideURL()
    {
        //构造函数
    }
    /// <summary>
    /// 上一页
    /// </summary>
    public string LastPage
    {
        get;
        set;
    }
    /// <summary>
    /// 下一页
    /// </summary>
    public string NextPage
    {
        get;
        set;
    }
    /// <summary>
    /// 首页
    /// </summary>
    public string IndexPage
    {
        get;
        set;
    }
    /// <summary>
    /// 第一页
    /// </summary>
    public string FirstPage
    {
        get;
        set;
    }
    /// <summary>
    /// 最后页
    /// </summary>
    public string EndPage
    {
        get;
        set;
    }
}
/// <summary>
/// 目录页信息类
/// </summary>
public class IndexInfo
{
    public string IndexID
    {
        get;
        set;
    }
    public string IndexURL
    {
        get;
        set;
    }
    public string BookName
    {
        get;
        set;
    }
    public string BookID
    {
        get;
        set;
    }
}
/// <summary>
/// 书籍基本信息类。
/// </summary>
/// <summary>
/// 书籍基本信息类。
/// </summary>
public class Book
{
    public string BookID
    {
        get;
        set;
    }
    public string BookName
    {
        get;
        set;
    }
    public string BookWriter
    {
        get;
        set;
    }
    public string BookRem
    {
        get;
        set;
    }
    public string TotalVisit
    {
        get;
        set;
    }
    public string WeekVisit
    {
        get;
        set;
    }
    public string BookState
    {
        get;
        set;
    }
    public string BookCover
    {
        get;
        set;
    }
    public string BookList
    {
        get;
        set;
    }
    public string BookType
    {
        get;
        set;
    }
    public string BookTypeID
    {
        get;
        set;
    }
    public string TotalRMD
    {
        get;
        set;
    }
    public string UserName
    {
        get;
        set;
    }
    public string AddTime
    {
        get;
        set;
    }
}

public class SiteInfo
{
    public string SiteID
    {
        get;
        set;
    }
    public string SiteName
    {
        get;
        set;
    }
    public string SearchURL
    {
        get;
        set;
    }
    public string SearchParam
    {
        get;
        set;
    }
    public string SiteEncode
    {
        get;
        set;
    }
    public string SiteMethod
    {
        get;
        set;
    }
    public string ResultReg
    {
        get;
        set;
    }
    //检索结果中书名、作者、目录的排序
    public string ResultOrder
    {
        get;
        set;
    }
        //检索结果中列表的格式
    public string ListFormat
    {
        get;
        set;
    }
    public string SiteRedirect
    {
        get;
        set;
    }
    //重定向时候的正则表达式
    public string ResultReg2
    {
        get;
        set;
    }
    public string ResultOrder2
    {
        get;
        set;
    }
    public string ListFormat2
    {
        get;
        set;
    }
}

public class BookSite
{

    public string SiteID
    {
        get;
        set;
    }
    public string SiteName
    {
        get;
        set;
    }
    public string BookFormat
    {
        get;
        set;
    }
    public string BookName
    {
        get;
        set;
    }
    public string BookWriter
    {
        get;
        set;
    }
    public string BookCover
    {
        get;
        set;
    }
    public string CoverFormat
    {
        get;
        set;
    }
    public string BookRem
    {
        get;
        set;
    }
    public string BookLastIndex
    {
        get;
        set;
    }

}

public class ChapterSearchType
{
    public const int Hot = 1;
    public const int Cold = 2;
    public const int BookShelf = 3;
    public const int Failed = 4;
}